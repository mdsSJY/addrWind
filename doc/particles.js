const particlesOptions = {
      particles: {
        number: {
          value: 80, // 粒子数量
          density: {
            enable: true,
            value_area: 800, // 粒子密度区域大小
          },
        },
        color: {
          value: '#ffffff', // 粒子颜色
        },
        shape: {
          type: 'circle', // 形状类型
        },
        size: {
          value: 3, // 粒子大小
          random: true,
          anim: {
            enable: false,
            speed: 4,
            size_min: 0.3,
          },
        },
        move: {
          enable: true,
          speed: 2, // 粒子移动速度
          direction: 'none', // 移动方向
          random: false,
          straight: false,
          out_mode: 'out', // 碰撞模式
          attract: {
            enable: false,
            rotateX: 600,
            rotateY: 1200,
          },
        },
      },
      interactivity: {
        detect_on: 'canvas', // 交互检测
        events: {
          onhover: {
            enable: true,
            mode: 'repulse', // 鼠标悬停时的效果
          },
          onclick: {
            enable: true,
            mode: 'push', // 点击时的效果
          },
        },
        modes: {
          grab: { distance: 400, line_linked: { opacity: 1 } },
          bubble: { distance: 400, size: 40, duration: 2, opacity: 8, speed: 3 },
          repulse: { distance: 200 },
          push: { particles_nb: 4 },
          remove: { particles_nb: 2 },
        },
      },
      retina_detect: true, // 是否启用视网膜适应
    };
