
export function Obj_isEmpty(obj: object) : boolean {
    return obj === null || (Object.keys(obj).length ===0 && obj.constructor === Object)
}

export function Obj_isNotEmpty(obj: object) : boolean {
    return !Obj_isEmpty(obj);
}

export function Str_isEmpty(str: any) : boolean {
    return str === null || str === undefined || str === '';
}

export const sleep = (ms: number) => new Promise(resolve => setTimeout(resolve, ms));

export const StrIsNotEmpty = ({str}: {
    str: string
}): boolean => !(str === null || str === undefined || str === '');

const svg = `
        <path class="path" d="
          M 30 15
          L 28 17
          M 25.61 25.61
          A 15 15, 0, 0, 1, 15 30
          A 15 15, 0, 1, 1, 27.99 7.5
          L 15 15
        " style="stroke-width: 4px; fill: rgba(0, 0, 0, 0)"/>
      `

export const tools = {
    getResultToTable: function (data: segAddrResponse): strPair[] {
        if (Obj_isEmpty(data) || Obj_isEmpty(data.resultAddress)) return [];
        const resultAddress = data.resultAddress;
        const names = Object.keys(resultAddress);
        const tableRes : strPair[] = [];
        for (const namesKey of names) {
            let temp:strPair = {
                data: (resultAddress as any)[namesKey],
                name: namesKey
            };
            tableRes.push(temp);
        }
        return tableRes;
    },
    formatJson: function (jsonString: Object): string {
        try {
            return JSON.stringify(jsonString, null, 2);
        } catch (error) {
            console.error('Error while formatting JSON:', error);
            return JSON.stringify(error, null, 2);
        }
    },
    svg,
}
