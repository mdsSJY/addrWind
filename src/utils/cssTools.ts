const cssTools = {
    rgb2rgba: (rgbString:string, alpha: number) : string => {
        try {
            const regex = /^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/;
            const match = rgbString.match(regex);
            const [r, g, b] = match.slice(1).map(Number);
            return `rgba(${r}, ${g}, ${b}, ${alpha})`;
        } catch (e) {
            return 'rgba(33,150,243, 0.22)';
        }
    },
}

export default cssTools;