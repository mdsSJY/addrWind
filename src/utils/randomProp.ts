import * as prop from './particleProperty'
import {random_list} from './particleProperty'
import * as utils from './utils';


export function getProperty() : any {
    const properties : any[] = [];
    const regex = /options[0-9]+/;
    for (const key in random_list) {
        if (regex.test(key) && utils.Obj_isNotEmpty(random_list[key])) {
            properties.push(random_list[key])
        }
    }
    if (properties.length === 0) return prop.options1;
    let index = getRandomRange(0, properties.length - 1);
    return properties[index];
}

export function getAppointProp() {
    return prop['options3'];
}

function getRandomRange(min: number, max: number) : number {
    if (min > max) {
        [min, max] = [max, min];
    }
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
