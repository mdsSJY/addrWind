import {addrService, entService} from '@/plugins/axios'

class GetParam {
    private readonly params:URLSearchParams;
    constructor() {
        this.params = new URLSearchParams();
    }

    public add(key:string, value:string) {
        this.params.append(key, value);
        return this;
    }

    public build() {
        return  { params: this.params };
    }

    static newIns() {
        return new GetParam()
    }
}

const ent = {
    refreshEntCache: async function () {
        return await (await entService().get('', {}))['data']
    },
    getEntTexture: async function(entName: string):Promise<any>{
        const options = GetParam.newIns()
            .add('entName', entName).build();
        return await entService().get(window.server.GetEntTexture, options);
    },
    getScanRes: async function(entName: string):Promise<any>{
        const options = GetParam.newIns()
            .add('entName', entName).build();
        return await (await entService().get(window.server.GetScanRes, options))['data'];
    },
    enterpriseServices: async function (entName: string):Promise<segEntResponse>{
        const data = {
            callType: 'EA',
            entName: entName
        }
        return (await entService().post('', data))['data'];
    }

}


const exportBtn = {
    // 刷新
    addressServices: async function (addr: string): Promise<any> {
        const data = {
            addrStr: addr,
            tag: 'FullAddress',
            standard: 'true'
        };
        return await addrService().post('', data);
    },
    // 分词请求按钮
    refreshPage: async function() {
        // window.location.reload();
        await addrService().get('', {});
    },
    // 地址的类型查询
    selectAType: async function (aType: string): Promise<any> {
        return await addrService().get(window.server.AddressTypeURL,
            GetParam.newIns().add('aType', aType).build());
    },
    // 获取prov数据
    selectProv: async function() : Promise<any> {
        return await addrService().get(window.server.GetProvURL, { });
    },
    // 获取city数据
    selectCity: async function(prov: string) : Promise<any> {
        const options = GetParam.newIns().add('prov', prov).build();
        return await addrService().get(window.server.GetCityURL, options);
    },
    // 获取dist数据
    selectDist: async function(prov: string, city: string) : Promise<any> {
        const options = GetParam.newIns()
            .add('prov', prov)
            .add('city', city)
            .build();
        return await addrService().get(window.server.GetDistURL, options);
    },
    // 获取town数据
    selectTown: async function(prov: string, city: string, dist: string) : Promise<any> {
        const options = GetParam.newIns()
            .add('prov', prov)
            .add('city', city)
            .add('dist', dist)
            .build();
        return await addrService().get(window.server.GetTownURL, options);
    },
    getAddrTexture: async function(address: any):Promise<any> {
        const options = GetParam.newIns()
            .add('address', address).build();
        return await addrService().get(window.server.GetTexture, options);
    },
    ent: ent,
}

export default exportBtn