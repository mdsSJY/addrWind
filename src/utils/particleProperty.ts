export const options1 = {
    // background: {
    //     color: {
    //         value: 'rgba(34,167,242,0.4)'
    //     }
    // },
    fpsLimit: 60,
    interactivity: {
        events: {
            onClick: {
                enable: true,
                mode: 'push'
            },
            onHover: {
                enable: true,
                mode: 'repulse'
            },
            resize: true
        },
        modes: {
            bubble: {
                distance: 400,
                duration: 2,
                opacity: 0.8,
                size: 40
            },
            push: {
                quantity: 4
            },
            repulse: {
                distance: 200,
                duration: 0.4
            }
        }
    },
    particles: {
        color: {
            value: '#22a7f2'
        },
        links: {
            color: '#ffffff',
            distance: 150,
            enable: true,
            opacity: 0.5,
            width: 1
        },
        collisions: {
            enable: true
        },
        move: {
            direction: 'none',
            enable: true,
            outModes: {
                default: 'bounce'
            },
            random: true,
            speed: 1,
            straight: false
        },
        number: {
            density: {
                enable: true,
                area: 1000
            },
            value: 50
        },
        opacity: {
            value: 0.5
        },
        shape: {
            type: 'circle'
        },
        size: {
            value: { min: 1, max: 5 }
        }
    },
    detectRetina: true
};

export const options2 = {
    fps_limit: 60,
    interactivity: {
        detect_on: "canvas",
        events: {
            onclick: { enable: true, mode: "push" },
            onhover: {
                enable: true,
                mode: "attract",
                parallax: { enable: false, force: 60, smooth: 10 }
            },
            resize: true
        },
        modes: {
            push: { quantity: 4 },
            attract: { distance: 200, duration: 0.4, factor: 5 }
        }
    },
    particles: {
        color: { value: "#fd6900" },
        line_linked: {
            color: "#ffffff",
            distance: 150,
            enable: true,
            opacity: 0.4,
            width: 1
        },
        move: {
            attract: { enable: false, rotateX: 600, rotateY: 1200 },
            bounce: false,
            direction: "none",
            enable: true,
            out_mode: "out",
            random: false,
            speed: 2,
            straight: false
        },
        number: { density: { enable: true, value_area: 1000 }, value: 50 },
        opacity: {
            anim: { enable: false, opacity_min: 0.1, speed: 1, sync: false },
            random: false,
            value: 0.5
        },
        shape: {
            character: {
                fill: false,
                font: "Verdana",
                style: "",
                value: "*",
                weight: "400"
            },
            image: {
                height: 100,
                replace_color: true,
                src: "images/github.svg",
                width: 100
            },
            polygon: { nb_sides: 5 },
            stroke: { color: "#000000", width: 0 },
            type: "circle"
        },
        size: {
            anim: { enable: false, size_min: 0.1, speed: 40, sync: false },
            random: true,
            value: 5
        }
    },
    polygon: {
        draw: { enable: false, lineColor: "#ffffff", lineWidth: 0.5 },
        move: { radius: 10 },
        scale: 1,
        type: "none",
        url: ""
    },
    retina_detect: true
}

export const options3 = {
    fpsLimit: 30,
    backgroundMode: {
        enable: true,
        zIndex: 10
    },
    particles: {
        number: {
            value: 10,
            density: {
                enable: true,
                area: 800
            }
        },
        color: {
            value: [
                "#3998D0",
                "#2EB6AF",
                "#A9BD33",
                "#FEC73B",
                "#F89930",
                "#F45623",
                "#D62E32",
                "#EB586E",
                "#9952CF"
            ]
        },
        destroy: {
            mode: "split",
            split: {
                count: 1,
                factor: {
                    value: 9,
                    random: {
                        enable: true,
                        minimumValue: 4
                    }
                },
                rate: {
                    value: 10,
                    random: {
                        enable: true,
                        minimumValue: 5
                    }
                },
                particles: {
                    collisions: {
                        enable: false
                    },
                    destroy: {
                        mode: "none"
                    },
                    life: {
                        count: 1,
                        duration: {
                            value: 1
                        }
                    }
                }
            }
        },
        shape: {
            type: "circle",
            stroke: {
                width: 0,
                color: "#000000"
            },
            polygon: {
                sides: 5
            },
            image: {
                src: "https://cdn.matteobruni.it/images/particles/github.svg",
                width: 100,
                height: 100
            }
        },
        opacity: {
            value: 1,
            random: false,
            animation: {
                enable: false,
                speed: 1,
                minimumValue: 0.1,
                sync: false
            }
        },
        size: {
            value: 15,
            random: {
                enable: true,
                minimumValue: 10
            },
            animation: {
                enable: false,
                speed: 40,
                minimumValue: 0.1,
                sync: false
            }
        },
        lineLinked: {
            enable: false,
            distance: 150,
            color: "#ffffff",
            opacity: 0.4,
            width: 1
        },
        collisions: {
            enable: true,
            mode: "destroy"
        },
        move: {
            enable: true,
            speed: 7,
            direction: "none",
            random: false,
            straight: false,
            out_mode: "out",
            attract: {
                enable: false,
                rotateX: 600,
                rotateY: 1200
            }
        }
    },
    interactivity: {
        detectsOn: "window",
        events: {
            onHover: {
                enable: false,
                mode: "repulse",
                parallax: {
                    enable: false,
                    force: 60,
                    smooth: 10
                }
            },
            onClick: {
                enable: true,
                mode: "push"
            },
            resize: true
        },
        modes: {
            grab: {
                distance: 400,
                line_linked: {
                    opacity: 1
                }
            },
            bubble: {
                distance: 400,
                size: 40,
                duration: 2,
                opacity: 0.8
            },
            repulse: {
                distance: 200
            },
            push: {
                particles_nb: 1
            },
            remove: {
                particles_nb: 2
            }
        }
    },
    detectRetina: true
};

export const options4 = {
    fpsLimit: 60,
    particles: {
        number: {
            value: 0,
            density: {
                enable: true,
                value_area: 800
            }
        },
        color: {
            value: "#ffff00"
        },
        shape: {
            type: "circle"
        },
        opacity: {
            value: 1,
            random: false,
            animation: {
                enable: true,
                speed: 0.5,
                minimumValue: 0,
                sync: false
            }
        },
        size: {
            value: 8,
            random: { enable: true, minimumValue: 4 },
            animation: {
                enable: false,
                speed: 20,
                minimumValue: 4,
                sync: false
            }
        },
        move: {
            enable: true,
            gravity: {
                enable: true,
                acceleration: -0.5
            },
            speed: 5,
            direction: "top",
            random: false,
            straight: false,
            outModes: {
                default: "destroy",
                bottom: "none"
            },
            attract: {
                enable: true,
                distance: 300,
                rotate: {
                    x: 600,
                    y: 1200
                }
            }
        }
    },
    interactivity: {
        detectsOn: "canvas",
        events: {
            resize: true
        }
    },
    detectRetina: true,
    emitters: [
        {
            direction: "top",
            particles: {
                color: "#f00"
            },
            rate: {
                quantity: 1,
                delay: 0.1
            },
            size: {
                width: 100,
                height: 10
            },
            position: {
                x: 50,
                y: 100
            }
        },
        {
            direction: "top",
            particles: {
                color: "#0f0"
            },
            rate: {
                quantity: 1,
                delay: 0.1
            },
            size: {
                width: 100,
                height: 10
            },
            position: {
                x: 50,
                y: 100
            }
        }
    ]
};

export const options5 = null;

export const options_exp1 = {
    fpsLimit: 120,
    particles: {
        number: {
            value: 100,
            density: {
                enable: false,
                value_area: 800
            }
        },
        color: {
            value: ["#5bc0eb", "#fde74c", "#9bc53d", "#e55934", "#fa7921"]
        },
        shape: {
            type: "circle"
        },
        opacity: {
            value: 1
        },
        size: {
            value: 32,
            random: {
                enable: true,
                minimumValue: 16
            }
        },
        line_linked: {
            enable: false,
            distance: 150,
            color: "#ffffff",
            opacity: 0.4,
            width: 1
        },
        move: {
            enable: true,
            speed: 1,
            direction: "none",
            random: false,
            straight: false,
            outMode: "out",
            bounce: false,
            warp: false,
            noise: {
                enable: true,
                delay: {
                    value: 0.1
                }
            }
        }
    },
    interactivity: {
        detect_on: "canvas",
        events: {
            onHover: {
                enable: true,
                mode: "push"
            },
            resize: true
        }
    },
    detectRetina: true,
    pauseOnBlur: true
};

export const random_list: any[] = [options1, options2, options3, options4];

