import { createStore } from 'vuex';

interface State {
    MasterWin: string,
    AddrSegWinState: string,
    winState: Map<string, object>,
}

const state : State = {
    MasterWin: 'MasterWin',// DebugWin,MasterWin,AddrTypeSelectWin,AdministrativeAreaWin
    AddrSegWinState: '',// '',wait,data,error
    winState: new Map(),

}

const mutations = {
    setMasterWin(state, component: string) {
        state.MasterWin = component;
    },
    setAddrSegWinState(state, currentState: string) {
        state.AddrSegWinState = currentState;
    },
    setWinState(state, winObject:object) {
        state.winState = winObject;
    }
}

const actions = {
    changeWin({commit}, component: string) {
        commit("setMasterWin", component);
    },
    changeAddrSegWinState({commit}, currentState: string) {
        commit("setAddrSegWinState", currentState);
    },
    commitWinState({commit}, winObject:object) {
        commit('setWinState', winObject);
    }

}

const getters = {
    getWinState(state, key:string) : object {
        return state.winState.get(key) ?? {};
    }
}

export default createStore({
    state,
    mutations,
    actions,
    getters,
});