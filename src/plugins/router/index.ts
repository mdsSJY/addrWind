import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";

export const routes: IRouteRecordRaw[] = [
  {
    path: "/worker/",
    name: "MasterWin",
    component: () => import("@/view/MasterWin.vue"),
    cnName: "地址分词",
  },
  {
    path: "/worker/EnterpriseWin",
    name: "EnterpriseWin",
    component: () => import("@/view/EnterpriseWin.vue"),
    cnName: "单位分词",
  },
  {
    path: "/tools",
    name: "tools",
    component: () => import("@/view/ToolsWin.vue"),
    children: [
      {
        path: "/worker/AddrTypeSelectWin",
        name: "AddrTypeSelectWin",
        cnName: "地址类型查询",
        component: () => import("@/view/tools/AddrTypeSelectWin.vue"),
      },
      {
        path: "/worker/AdministrativeAreaWin",
        name: "AdministrativeAreaWin",
        cnName: "行政区划查询",
        component: () => import("@/view/tools/AdministrativeAreaWin.vue"),
      },
      {
        path: "/worker/RuleConsoleWin",
        name: "RuleConsoleWin",
        cnName: "分词规则配置",
        component: () => import("@/view/tools/RuleConsoleWin.vue"),
      },
      {
        path: "/worker/EntTextureWin",
        name: "EntTextureWin",
        cnName: "单位纹理查看",
        component: () => import("@/view/tools/EntTextureWin.vue"),
      },
    ],
    cnName: "",
  },
  {
    path: "/work/DebugWin",
    name: "DebugWin",
    component: () => import("@/view/DebugWin.vue"),
    cnName: "DebugConsole",
  },
];

function getChildRoutes(child: IRouteRecordRaw[]): RouteRecordRaw[] {
  if (child) {
    return child.map((child: IRouteRecordRaw) => {
      return {
        path: child.path,
        name: child.name,
        component: child.component,
      };
    });
  }
  return undefined;
}

function getRoute(): RouteRecordRaw[] {
  return routes.map((item) => {
    return {
      path: item.path,
      name: item.name,
      component: item.component,
      children: getChildRoutes(item.children),
    };
  });
}
// 创建路由实例
const router = createRouter({
  history: createWebHistory(), // 或者使用createWebHashHistory()创建基于哈希的历史模式
  routes: getRoute(),
});

export default router;
