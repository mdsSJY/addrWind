import router from "./plugins/router/index";
import store from "./plugins/store";
import "./assets/index.css";
import Particles from "vue3-particles";
import ElementPlus from "element-plus";
import "element-plus/dist/index.css";

import App from "./App.vue";

const app = createApp(App);

// 注册路由
app.use(router);

// 状态管理库vuex
app.use(store);

// 粒子特效
app.use(Particles);

// element ui
app.use(ElementPlus);

app.mount("#app");
