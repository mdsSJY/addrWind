window.server = {
    // 地址
    // "http://localhost:8095"
    // AddrSegBaseURL: "http://192.168.123.204:8037/AddrSegWin",
    AddrSegBaseURL: "https://www.sjy.asia/AddrSegWin",
    DebugURL: "https://localhost:8094",
    AddressTypeURL: "/AddrType",
    GetProvURL: "/GetProv",
    GetCityURL: "/GetCity",
    GetDistURL: "/GetDist",
    GetTownURL: "/GetTown",
    GetTexture: "/GetTexture",


    // 单位
    EntBaseURL: "https://www.sjy.asia/jENTProcWin",
    // EntBaseURL: "http://192.168.123.204:7037/jENTProcWin",
    GetEntTexture: "/GetEntTexture",
    GetScanRes: "/GetScanRes",
    Version: "ver1.0.0.5_SnapShot"
}
