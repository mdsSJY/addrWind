window.server = {
  // 地址
  // 代理服务器用的https，所以必需用https
  AddrSegBaseURL: "https://sjy.asia/AddrSegWin",
  AddressTypeURL: "/AddrType",
  GetProvURL: "/GetProv",
  GetCityURL: "/GetCity",
  GetDistURL: "/GetDist",
  GetTownURL: "/GetTown",
  GetTexture: "/GetTexture",

  // 单位
  EntBaseURL: "https://sjy.asia/jENTProcWin",
  GetEntTexture: "/GetEntTexture",
  GetScanRes: "/GetScanRes",
  Version: "latest",
};
