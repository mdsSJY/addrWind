import fs from "fs-extra";
import { join } from "path";

const root = process.cwd();
const dist_path = join(root, "dist");
const build_path = join(root, ".publish", "dist");
const docker_config = join(root, ".publish", "docker-env", "config.js");
const docker_build = join(build_path, "worker", "config.js");
// console.log(`docker_config: ${docker_config}`);

fs.remove(build_path)
  .then(() => {
    return fs.copy(dist_path, build_path);
  })
  .then(() => {
    console.log("copy dist success.");
    fs.copy(docker_config, docker_build)
      .then(() => {
        console.log("copy docker config success.");
      })
      .catch((err: any) => {
        console.error(err);
      });
  })
  .catch((err: any) => {
    console.error(err);
  });
