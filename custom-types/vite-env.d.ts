/// <reference types="vite/client" />
/// <reference types="vue-router/dist/vue-router.d.ts" />

declare module "*.vue" {
  import type { DefineComponent } from "vue";
  // eslint-disable-next-line @typescript-eslint/no-explicit-any, @typescript-eslint/ban-types
  const component: DefineComponent<{}, {}, any>;
  export default component;
}

declare module "element-plus";

declare module "js-cookie";

declare module "vuex";

declare module "vue3-particles";
