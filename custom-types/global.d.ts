// custom-types/global.d.ts
export {}; // 将文件转换为模块
declare global {
  interface ResultAddressPojo {
    provName: string;
    cityName: string;
    distName: string;
    townName: string;
    road: string;
    poiStr: string;
    roadDetail: string;
    poiDetail: string;
    other: string;

    provType: string;
    cityType: string;
    distType: string;
    townType: string;
    roadType: string;
    poiType: string;

    originalValue: string;
  }

  interface segAddrResponse {
    statusCode: bigint;
    message: string;
    labeledAddress: string;
    resultAddress: ResultAddressPojo;
  }

  interface segEntResponse {
    statusCode: string;
    message: string;
    entType: string;
    splitResult: string;
    shortName: string;
    parseResult: ResultEnterprisePojo;
  }

  interface ResultEnterprisePojo {
    isNormal: boolean;
    attr0: string;
    attr1: string;
    attr2: string;
    attr3: string;
    attr4: string;
    attr5: string;
    attr6: string;
    attr7: string;
    attr8: string;
    attr9: string;
    other: string;
  }

  interface strPair {
    data: string;
    name: string;
  }

  interface AxiosObject {
    get: (url: string, options: { params?: URLSearchParams }) => Promise<any>;
    post: (url: string, data: any) => Promise<any>;
  }

  interface SwitchBtn1Name {
    name1: string;
    name2: string;
  }

  interface IRouteRecordRaw {
    path: string;
    name: string;
    component: any;
    children?: IRouteRecordRaw[];
    cnName?: string;
  }
  interface Window {
    server: {
      AddrSegBaseURL: string;
      AddressTypeURL: string;
      GetProvURL: string;
      GetCityURL: string;
      GetDistURL: string;
      GetTownURL: string;
      GetTexture: string;

      EntBaseURL: string;
      GetEntTexture: string;
      GetScanRes: string;
      Version: string;
    };
  }
}
