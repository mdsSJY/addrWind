# 使用Nginx作为运行时镜像
FROM nginx:1.23.0
# 清除默认的 Nginx 配置文件
RUN rm -rf /usr/share/nginx/html/*
# 导入nginx配置文件
COPY ./doc/nginx.conf /etc/nginx/conf.d/default.conf

# 从构建阶段拷贝编译好的前端应用程序到运行时镜像
COPY ./dist /usr/share/nginx/html

# 暴露容器接口
EXPOSE 80

# 启动 Nginx
CMD ["nginx", "-g", "daemon off;"]
LABEL authors="SongJuYi"

# ---------------------------------

## 使用官方Node.js 作为base镜像
#FROM node:20.12.1 as build-stage
#
## 设置工作目录
#WORKDIR /app
#
## 复制 package.json 和 package-lock。json 到容器内部
## COPY package*.json ./
#
## 拷贝所有文件到工作目录
#COPY . .
#
## 下载依赖。
#RUN npm install
#
## 构建应用程序
#RUN npm run build
#
## 使用Nginx作为运行时镜像
#FROM nginx:1.23.0
#
## 清除默认的 Nginx 配置文件
#RUN rm -rf /usr/share/nginx/html/*
#
## 从构建阶段拷贝编译好的前端应用程序到运行时镜像
#COPY --from=build-stage /app/dist /usr/share/nginx/html
#
## 暴露容器接口
#EXPOSE 80
#
## 启动 Nginx
#CMD ["nginx", "-g", "daemon off;"]
#LABEL authors="SongJuYi"
#
## 显示系统的实时状态
## ENTRYPOINT ["top", "-b"]