# 一个简单的前端窗口

### docker部署
[Dockerfile](Dockerfile)
```bash
# 构建镜像
docker build -t demo

# 镜像启动
docker run -p 7777:80 demo
```

### 最新的Dockerfile build流程
1. npm install
2. npm run build
3. docker build -t demo
4. docker-compose up


### nodejs 启动  

- 需要nodejs

- 文件结构
```bash
├───dist
│   ├───package.json
│   ├───package-lock.json
│   ├───vite.config.ts
│   ├───vite-env.d.ts
│   ├───components.d.ts
```
```bash
# dist所在根目录下
# 安装依赖
npm install
# 启动
npm run preview
```

### 效果演示
[http://sjy.asia/worker](http://sjy.asia/worker)  
特此记录。并开源。

### 鸣谢
感谢Vue3,以及本项目中使用到的所有开源工具/软件/框架。